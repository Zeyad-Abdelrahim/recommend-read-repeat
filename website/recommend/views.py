from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.shortcuts import render, get_object_or_404, redirect
from .forms import *
from django.http import Http404
from .models import Book, Myrating, Mylist
from django.db.models import Q
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.db.models import Case, When
import pandas as pd

# Create your views here.
def index(request):
    books = Book.objects.all()
    query = request.GET.get('q')

    if query:
        books = Book.objects.filter(Q(title__icontains=query)).distinct()
        return render(request, 'list.html', {'books': books})
    
    return render(request, 'list.html', {'books': books})


def detail(request, book_id):
    if not request.user.is_authenticated:
        return redirect("login")
    if not request.user.is_active:
        raise Http404
    books = get_object_or_404(Book, id=book_id)
    book = Book.objects.get(id=book_id)
    
    temp = list(Mylist.objects.all().values().filter(book_id=book_id,user=request.user))
    
    if temp:
        update = temp[0]['read']
    else:
        update = False
    if request.method == "POST":

        # For my list
        if 'read' in request.POST:
            read_flag = request.POST['read']
            if read_flag == 'on':
                update = True
            else:
                update = False
            if Mylist.objects.all().values().filter(book_id=book_id,user=request.user):
                Mylist.objects.all().values().filter(book_id=book_id,user=request.user).update(read=update)
            else:
                q=Mylist(user=request.user,book=book,read=update)
                q.save()
            if update:
                messages.success(request, "Book added to your list!")
            else:
                messages.success(request, "Book removed from your list!")

            
        # For rating
        else:
            rate = request.POST.get('rating')
            if rate == None:
                messages.add_message(request, 20, "To add the box to your list please check the below check box")
                
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            if Myrating.objects.all().values().filter(book_id=book_id,user=request.user):
                Myrating.objects.all().values().filter(book_id=book_id,user=request.user).update(rating=rate)
            else:
                q=Myrating(user=request.user,book=book,rating=rate)
                q.save()

            messages. success(request, "Rating has been submitted!")

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    out = list(Myrating.objects.filter(user=request.user.id).values())

    # To display ratings in the Book detail page
    book_rating = 0
    rate_flag = False
    for each in out:
        if each['book_id'] == book_id:
            book_rating = each['rating']
            rate_flag = True
            break

    context = {'books': books,'book_rating':book_rating,'rate_flag':rate_flag,'update':update}
    return render(request, 'detail.html', context)

def read(request):

    if not request.user.is_authenticated:
        return redirect("login")
    if not request.user.is_active:
        raise Http404

    books = Book.objects.filter(mylist__read=True,mylist__user=request.user)
    query = request.GET.get('q')

    if query:
        books = Book.objects.filter(Q(title__icontains=query)).distinct()
        return render(request, 'read.html', {'books': books})

    return render(request, 'read.html', {'books': books})


def get_similar(book_name, rating,corrMatrix):
    similar_ratings = corrMatrix[book_name]*(rating-2.5)
    similar_ratings = similar_ratings.sort_values(ascending=False)
    return similar_ratings



def recommend(request):

    if not request.user.is_authenticated:
        return redirect("login")
    if not request.user.is_active:
        raise Http404


    book_rating=pd.DataFrame(list(Myrating.objects.all().values()))
    
    
    new_user=book_rating.user_id.unique().shape[0]
    current_user_id= request.user.id
	# if new user not rated any book
    if current_user_id>new_user:
        book=Book.objects.get(id=19)
        q=Myrating(user=request.user,book=book,rating=0)
        q.save()


    userRatings = book_rating.pivot_table(index=['user_id'],columns=['book_id'],values='rating')
    userRatings = userRatings.fillna(0,axis=1)
    
    corrMatrix = userRatings.corr(method='pearson')
    
    
    user = pd.DataFrame(list(Myrating.objects.filter(user=request.user).values())).drop(['user_id','id'],axis=1)
    user_filtered = [tuple(x) for x in user.values]
    book_id_read = [each[0] for each in user_filtered]
    
    similar_books = pd.DataFrame()
    for book,rating in user_filtered:
        similar_books = similar_books.append(get_similar(book,rating,corrMatrix),ignore_index = True)


    books_id = list(similar_books.sum().sort_values(ascending=False).index)
    books_id_recommend = [each for each in books_id if each not in book_id_read]
    preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(books_id_recommend)])
    book_list=list(Book.objects.filter(id__in = books_id_recommend).order_by(preserved)[:10])

    context = {'book_list': book_list}
    return render(request, 'recommend.html', context)


# Register user
def signUp(request):
    form = UserForm(request.POST or None)

    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect("index")

    context = {'form': form}

    return render(request, 'signUp.html', context)



# Login User
def Login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect("index")
            else:
                return render(request, 'login.html', {'error_message': 'Your account disable'})
        else:
            return render(request, 'login.html', {'error_message': 'Invalid Login'})

    return render(request, 'login.html')


# Logout user
def Logout(request):
    logout(request)
    return redirect("login")