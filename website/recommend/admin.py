from django.contrib import admin
from .models import Book, Mylist, Myrating

# Register your models here.

admin.site.register(Book)
admin.site.register(Mylist)
admin.site.register(Myrating)
