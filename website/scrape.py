# scraper.py
import re
from urllib.request import urlopen
import pandas as pd
import requests
from bs4 import BeautifulSoup
import os
import django
from django.core.files.base import ContentFile
from io import BytesIO
from django.core.files.images import ImageFile


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')
django.setup()

# import the Book model
from recommend.models import Book

# define the web scraping code here

url = 'https://printige.net/product-category/ai-data-science/machine-learning/'


title_class = 'woocommerce-LoopProduct-link woocommerce-loop-product__link'
author_class = ''
book_logo_class = 'attachment-woocommerce_thumbnail size-woocommerce_thumbnail'





page = requests.get(url)
html = page.text

soup = BeautifulSoup(html, 'html.parser')

titles_arr = []
for a in soup.findAll('a', attrs={'class':title_class}):
    titles_arr.append(a.text)

# books_logos = []
# for i, a in enumerate(soup.findAll('img', class_= book_logo_class)):
#     img_url = a['src'][0]
#     if img_url == 'h':
#         books_logos.append(a['src'])



# books_data = pd.DataFrame({'Title':titles_arr, 'Logos': books_logos})


# print(books_data)


# for index, book_data in books_data.iterrows():
#     image_url = book_data['Logos']
#     response = requests.get(image_url)
#     book_logo = ContentFile(response.content)
    
#     book = Book(
#         title=book_data['Title'],
#         author='Unknown',
#         price=0
#     )
#     book.save()
#     print(image_url[-4:])
#     book.book_logo.save(book_data['Title'] + "." + image_url[-3:], book_logo, save=True)
# print('Books added to the database')

books_logos = []
for i, a in enumerate(soup.findAll('img', class_=book_logo_class)):
    img_url = a['src']
    if img_url.startswith('http'):
        books_logos.append(img_url)

books_data = pd.DataFrame({'Title': titles_arr, 'Logos': books_logos})

for index, book_data in books_data.iterrows():
    image_url = book_data['Logos']
    response = requests.get(image_url)
    image = ImageFile(BytesIO(response.content), name=f"{book_data['Title']}.jpg")
    
    book = Book(
        title=book_data['Title'],
        author='Unknown',
        price=0,
        book_logo=image,
    )
    book.save()
    print(f"Book '{book.title}' saved with image '{book.book_logo.name}'")