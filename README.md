# Recommend Read Repeat

Recommend Read Repeat is a web application built with Django and Python that allows users to order books and receive personalized recommendations based on their reading history.

**Features**:-
- Web scraping to add new books to the database
- User authentication and authorization
- Personalized book recommendations
- Database modeling for user profiles and book data
- Continuous integration and deployment using GitLab
- Responsive web design for mobile and desktop devices

## Getting Started
To get started with Recommend Read Repeat, follow these steps:

Clone the repository: <br>
`git clone https://gitlab.com/your-username/recommend-read-repeat.git`

Install the required packages:<br>
`pip install -r requirements.txt`

Set up the database:<br>
`python manage.py migrate`

Create a superuser account:<br>
`python manage.py createsuperuser`

Run the development server:<br>
`python manage.py runserver`

Access the web application at <br>
http://localhost:8000

You can add books manually from the admin panel found at:- <br>
http://localhost:8000/admin

Sign in with superuser then add as many books as you like.
Or you can scrap the website you like using the scrape.py script, add the url for the website you would like to scrape
and modify the tags you are searching for.


## Contributing
Contributions to Recommend Read Repeat are welcome! <br>
To contribute, please follow these steps:

- Fork the repository.
- Create a new branch for your feature or bug fix.
- Make your changes and commit them with descriptive messages.
- Push your changes to your fork.
- Submit a merge request to the main branch of the original repository.



## Contact
If you have any questions or suggestions for Recommend Read Repeat, please contact us through [this email](zeyadomar.4217@gmail.com). We would love to hear from you!
